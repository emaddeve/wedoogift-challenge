package wedoogit.back.end.java.challenge.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "COMPANY")
@Getter
@Setter
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private Long COMPANYID;
	
	@Column(name = "COMPANY_NAME")
	private String companyName;
	
	@Column(name = "BALANCE")
	private Integer balance;
}
