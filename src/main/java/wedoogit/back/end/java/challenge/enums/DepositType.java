package wedoogit.back.end.java.challenge.enums;

/**
 * type of balance that a company can distribute  to a user
 * @author ear
 *
 */
public enum DepositType {

	GIFT,MEAL
}
