package wedoogit.back.end.java.challenge.controllers;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wedoogit.back.end.java.challenge.services.IBalanceService;

@RestController
@RequestMapping(value ="/balance/")
public class BalanceController {
	
	@Autowired
	IBalanceService service;
	
	/**
	 * get the total balance by user Id if user doesn't exist or has no balance return 0
	 * @param userId
	 * @return total user balance
	 */
	@GetMapping(value = "{userId}")
	public Integer getBalanceByUser(@PathVariable final Long userId) {
		return service.getBalanceByUserId(userId);
	}

}
