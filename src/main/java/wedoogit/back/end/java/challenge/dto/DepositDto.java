package wedoogit.back.end.java.challenge.dto;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;
import wedoogit.back.end.java.challenge.enums.DepositType;

/**
 * 
 * @author ear
 *
 */
@Getter
@Setter
public class DepositDto {

	@NotNull
	private DepositType depositType;
	@NotNull
	private Integer depositAmount;
	@NotNull
	private UserDto beneficiary;
	@NotNull
	private String companyName;
}
