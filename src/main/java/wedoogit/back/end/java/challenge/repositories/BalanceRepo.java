package wedoogit.back.end.java.challenge.repositories;

import java.util.List;

import org.hibernate.annotations.Where;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wedoogit.back.end.java.challenge.entities.Balance;

@Repository
public interface BalanceRepo extends JpaRepository<Balance,Long>{

	List<Balance> findByUserId(Long userId);
}
