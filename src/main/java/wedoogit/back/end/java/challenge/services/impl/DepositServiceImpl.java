package wedoogit.back.end.java.challenge.services.impl;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wedoogit.back.end.java.challenge.dto.DepositDto;
import wedoogit.back.end.java.challenge.entities.Balance;
import wedoogit.back.end.java.challenge.entities.Company;
import wedoogit.back.end.java.challenge.entities.User;
import wedoogit.back.end.java.challenge.repositories.BalanceRepo;
import wedoogit.back.end.java.challenge.repositories.CompanyRepo;
import wedoogit.back.end.java.challenge.repositories.UserRepo;
import wedoogit.back.end.java.challenge.services.IDepositService;
import wedoogit.back.end.java.challenge.transformers.DepositBalanceTransformer;

@Service
public class DepositServiceImpl implements IDepositService {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	BalanceRepo balanceRepo;
	
	@Autowired
	CompanyRepo companyRepo;
	
	
	@Override
	public void putInBalance(DepositDto depositDto) {
		
		
		
		if(isUserExist(depositDto.getBeneficiary().getUserId()) || checkCompanyBalance(depositDto.getCompanyName(),depositDto.getDepositAmount())) {
		
		Balance balance = DepositBalanceTransformer.transformDepositDtoToBalance(depositDto);
		balanceRepo.save(balance);
		}
	}

	/**
	 * check if user exist in the DB by userId
	 * @param userId
	 * @return boolean
	 */
	protected boolean isUserExist(Long userId) {
		Optional<User> user = userRepo.findById(userId);
		return user.isPresent();
	}

	/**
	 * check if company exist and have enough balance to make the transaction
	 * @param companyName
	 * @param amount
	 * @return
	 */
	protected boolean checkCompanyBalance(String companyName, Integer amount) {
		Optional<Company> company = companyRepo.findByCompanyName(companyName);
		if((company.isPresent() && isCompanyBalanceAllowed(company.get().getBalance(),amount))) {
			Integer newCompanyBalance = company.get().getBalance() - amount;
			company.get().setBalance(newCompanyBalance);
			companyRepo.save(company.get());
			return true;
		}
		return false;
	}


/**
 * check if company balance is greater than the amount requested
 * @param balance
 * @param amount
 * @return
 */
	protected boolean isCompanyBalanceAllowed(Integer balance, Integer amount) {
		return balance > amount;
	}

}
