package wedoogit.back.end.java.challenge.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wedoogit.back.end.java.challenge.dto.DepositDto;
import wedoogit.back.end.java.challenge.services.IDepositService;

@RestController
@RequestMapping(value ="/deposit")
public class DepositsController {

	@Autowired
	IDepositService service;
	
	/**
	 * Add new balance to user and cut it down from company balance
	 * @param depositDto
	 * @throws FunctionalException 
	 */
	@PostMapping
	public void deposit(@Validated @RequestBody DepositDto depositDto){
		
		service.putInBalance(depositDto);

	}
}
