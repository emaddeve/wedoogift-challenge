package wedoogit.back.end.java.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wedoogit.back.end.java.challenge.entities.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

}
