package wedoogit.back.end.java.challenge.transformers;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;

import wedoogit.back.end.java.challenge.dto.DepositDto;
import wedoogit.back.end.java.challenge.entities.Balance;
import wedoogit.back.end.java.challenge.enums.DepositType;

public final class DepositBalanceTransformer {

	private DepositBalanceTransformer() {
		throw new IllegalStateException("Utility class");
	}
	
	/**
	 * transfrer DepositDto object to Balance object
	 * @param depositDto
	 * @return Balance
	 */
	public static Balance transformDepositDtoToBalance(DepositDto depositDto) {
		Balance balance = null;
		if(depositDto!=null) {
			balance = new Balance();
		balance.setAmount(depositDto.getDepositAmount());
		balance.setBalanceDate(balanceDateByGiftType(depositDto.getDepositType()));
		balance.setUserId(depositDto.getBeneficiary().getUserId());
		}
		return balance;
	}

	/**
	 * if depositType equal to Meal change date to the last day of february of next year
	 * else add one year
	 * @param depositType
	 * @return LocalDate
	 */
	protected static LocalDate balanceDateByGiftType(DepositType depositType) {
		LocalDate balanceValidDate = LocalDate.now().plusYears(1);
		if(DepositType.MEAL.equals(depositType)) {
			 balanceValidDate = balanceValidDate.withMonth(Month.FEBRUARY.getValue()).with(TemporalAdjusters.lastDayOfMonth());
		}
		return balanceValidDate;
	}
}
