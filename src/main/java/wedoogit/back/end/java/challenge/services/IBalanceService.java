package wedoogit.back.end.java.challenge.services;

/**
 * 
 * @author ear
 *
 */
public interface IBalanceService {

	/**
	 * get the total balance for a specific user
	 * @param userId
	 * @return total balance
	 */
	public Integer getBalanceByUserId(Long userId);
}
