package wedoogit.back.end.java.challenge.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ear
 *
 */
@Getter
@Setter
public class UserDto {

	private String userName;
	private Long userId;
}
