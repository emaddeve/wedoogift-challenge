package wedoogit.back.end.java.challenge.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wedoogit.back.end.java.challenge.entities.Company;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Long>{

	Optional<Company> findByCompanyName(String companyName);

}
