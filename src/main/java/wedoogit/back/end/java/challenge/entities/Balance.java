package wedoogit.back.end.java.challenge.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.AccessLevel;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "BALANCE")
@Getter
@Setter
@Where(clause = "balancedate > sysdate()")
public class Balance {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private Long balanceId;
	
	@Column(name = "USERID")
	private Long userId;
	
	@Column(name = "AMOUNT")
	private Integer amount;
	
	@Column(name = "BALANCEDATE")
	private LocalDate balanceDate;
	
}
