package wedoogit.back.end.java.challenge.services;

import wedoogit.back.end.java.challenge.dto.DepositDto;

public interface IDepositService {

	/**
	 * Add new balance to user and cut it down from company balance
	 * @param depositDto
	 * @throws FunctionalException 
	 */
	public void putInBalance(DepositDto depositDto) ;
}
