package wedoogit.back.end.java.challenge.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wedoogit.back.end.java.challenge.entities.Balance;
import wedoogit.back.end.java.challenge.repositories.BalanceRepo;
import wedoogit.back.end.java.challenge.services.IBalanceService;

@Service
public class BalanceServiceImpl implements IBalanceService {

	@Autowired
	BalanceRepo balanceRepo; 
	
	@Override
	public Integer getBalanceByUserId(Long userId) {
		
		List<Balance> balanceList = balanceRepo.findByUserId(userId);
		if(!balanceList.isEmpty()) {
			return balanceList.stream().map(e-> e.getAmount()).reduce(0,Integer::sum);
		}
		return 0;
	}

}
