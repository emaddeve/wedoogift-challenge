package wedoogit.back.end.java.challenge.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import wedoogit.back.end.java.challenge.services.IBalanceService;

@RunWith(SpringRunner.class)
@WebMvcTest(BalanceController.class)
public class BalanceControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    IBalanceService service;
    
    @Test
    public void getBalanceByUserOk() throws Exception {
    	
    	//Given
    	Integer balance = 100;
    	
    	//When
        Mockito.when(this.service.getBalanceByUserId(Mockito.anyLong()))
        .thenReturn(balance);
    	  
        //Do
    	mvc.perform(get("/balance/1")
    		      .contentType(MediaType.APPLICATION_JSON))
    		      .andExpect(status().isOk())
    		      .andExpect(content().json("100"));
    }
    
    @Test
    public void getBalanceByUserBadRequest() throws Exception {
    	
    	//Given
    	Integer balance = 100;
    	
    	//When
        Mockito.when(this.service.getBalanceByUserId(Mockito.anyLong()))
        .thenReturn(balance);
    	  
        //Do
    	mvc.perform(get("/balance/d")
    		      .contentType(MediaType.APPLICATION_JSON))
    	.andExpect(status().isBadRequest());;
    }
}
