package wedoogit.back.end.java.challenge.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class BalanceServiceImplTest {

	
	@Autowired
	BalanceServiceImpl service;
	
	@Test
	public void getBalanceByUserIdTestOK() {
		
		Integer balance = service.getBalanceByUserId(1l);
		assertEquals(balance, 100);
	}
	
	@Test
	public void should_return_0_as_user_not_exist() {
		
		Integer balance = service.getBalanceByUserId(2l);
		assertEquals(balance, 0);
	}
}
