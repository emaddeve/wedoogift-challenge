package wedoogit.back.end.java.challenge.controllers;

import static org.hamcrest.CoreMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import wedoogit.back.end.java.challenge.dto.DepositDto;
import wedoogit.back.end.java.challenge.dto.UserDto;
import wedoogit.back.end.java.challenge.enums.DepositType;
import wedoogit.back.end.java.challenge.services.IDepositService;

@RunWith(SpringRunner.class)
@WebMvcTest(DepositsController.class)
public class DepositControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    IDepositService service;
    
    @Test
    public void depositOk() throws Exception {
    	
    	//Given
    	DepositDto depositDto = new DepositDto();
    	depositDto.setCompanyName("google");
    	depositDto.setDepositAmount(100);
    	depositDto.setDepositType(DepositType.MEAL);
    	UserDto userDto = new UserDto();
    	userDto.setUserId(1l);
    	userDto.setUserName("emad");
    	depositDto.setBeneficiary(userDto);
    	
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer();
        String requestJson=ow.writeValueAsString(depositDto );
    	//When
        doNothing().when(service).putInBalance(Mockito.any());

        //Do
    	mvc.perform(post("/deposit").content(requestJson)
  		      .contentType(MediaType.APPLICATION_JSON))
  		      .andExpect(status().isOk());
    	
    }
    
}
