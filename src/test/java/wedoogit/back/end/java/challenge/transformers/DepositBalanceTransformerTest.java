package wedoogit.back.end.java.challenge.transformers;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import wedoogit.back.end.java.challenge.dto.DepositDto;
import wedoogit.back.end.java.challenge.dto.UserDto;
import wedoogit.back.end.java.challenge.entities.Balance;
import wedoogit.back.end.java.challenge.enums.DepositType;

@RunWith(SpringRunner.class)
public class DepositBalanceTransformerTest {

	@Test
	public void transformDepositDtoToBalance() {
    	//Given
    	DepositDto depositDto = new DepositDto();
    	depositDto.setCompanyName("google");
    	depositDto.setDepositAmount(100);
    	depositDto.setDepositType(DepositType.MEAL);
    	UserDto userDto = new UserDto();
    	userDto.setUserId(1l);
    	userDto.setUserName("emad");
    	depositDto.setBeneficiary(userDto);
    	
    	Balance balance  = DepositBalanceTransformer.transformDepositDtoToBalance(depositDto);
    	
    	assertThat(balance).isNotNull();
	}
	
	@Test
	public void should_return_null_object_when_param_null() {
		Balance balance  = DepositBalanceTransformer.transformDepositDtoToBalance(null);
    	
    	assertThat(balance).isNull();
	}
	
	@Test
	public void balanceDate_type_gift_Test() {
		LocalDate givenDate = 	 LocalDate.now().plusYears(1);
		LocalDate date = DepositBalanceTransformer.balanceDateByGiftType(DepositType.GIFT);
		assertEquals(date, givenDate);
	}
	
	@Test
	public void balanceDate_type_meal_Test() {
		LocalDate givenDate = 	 LocalDate.now().plusYears(1).withMonth(Month.FEBRUARY.getValue()).with(TemporalAdjusters.lastDayOfMonth());
		LocalDate date = DepositBalanceTransformer.balanceDateByGiftType(DepositType.MEAL);
		assertEquals(date, givenDate);
	}
}
